use strict;
use warnings;
use Data::Dumper;

###

my $budget_file =$ARGV[0];
my $entries_file =$ARGV[1];

print "\n";
my $hash_ref = open_files("Budget",$budget_file);
my $entry_hash_ref = open_files("Entries",$entries_file);
print Dumper \$entry_hash_ref;

## Need to create sub to subtract Entries from Budget.
print_hash("Total of Entries",$entry_hash_ref);

open_detail_budget("Budget Detail",$budget_file);
print "\n";
my $make_entries_href = make_entries_hash($entries_file);

report_status("Current Status",$hash_ref,$entry_hash_ref,$make_entries_href);

# Subs
#####
sub make_entries_hash{
my($file) = @_;
my $title = "make_entries_hash";
###
	print "----< $title >--------\n";
	my %hash = ();
	my $total = 0;
	my $ii = 0;
	open ( my $fh, "<", $file ) || die "Flaming death: $?\n";
	while (<$fh>){
	    my $line = $_;
	    if ( $line =~ m/^#+/ ) { #print "Skipping header.\n";
				      next; };
	    chomp($line);
	    print "\$line: $line\n";
	    my($tag,$note,$amount) = split(/\,/,$line);
	     $ii++;
	     print "$ii: $tag = $amount\n";
	     $total += $amount;
	     if( $hash{$tag} ){
		 $hash{$tag} .= "|".$line ;
		} else {
	     $hash{$tag} =  $line ;
	     }
	    }
	     print "Total = $total\n\n";
          print "DUMPER in make_entries_hash\n";
          print Dumper \%hash;
	  return(\%hash);
          close $fh;
###

};

sub report_status {
# make entries against budget with entries
my($title,$budget_href,$entry_href,$make_entries_href) = @_;
my $ii = 0;
my $total = 0;
print "-----< $title >----\n";
for my $entry ( sort keys %{ $budget_href } ){
   $ii++;
   print "$ii, $entry, ${$budget_href}{$entry}\n";
   #print "  : $entry = ${$entry_href}{$entry}\n";
        
   $total = ${$budget_href}{$entry} - ${$entry_href}{$entry};
   my $entries = ${$make_entries_href}{$entry};
   $entries =~ s/\|/\n/;
   print "$entries\n";
   print "  ,$entry, ${$entry_href}{$entry}\n";
   print "  ,total, $total\n\n";;
 }
};

sub print_hash {
# sub to print hash
my($title,$hashref) = @_;
my $ii = 0;
my $total = 0;
print "----< $title >--------\n";
for my $entry ( sort keys %{ $hashref } ){
   $ii++;
   print "$ii: $entry = ${$hashref}{$entry}\n";
   $total += ${$hashref}{$entry};
 }
   print "Total: $total\n";
};

sub open_detail_budget {
my($title,$file) = @_;
print "\n----< $title >--------\n";
my %hash = ();
my $total = 0;
my $ii = 0;
open ( my $fh, "<", $file ) || die "Flaming death: $?\n";
while (<$fh>){
    my $line = $_;
    if ( $line =~ m/^#+/ ) { #print "Skipping header.\n"; 
                              next; };
    chomp($line);
    #print "\$line: $line\n";
    my($tag,$note,$amount) = split(/\,/,$line);
     $ii++;
     print "$ii: $tag = $note, $amount\n";
     $total += $amount;
     if( $hash{$tag} ){
         $hash{$tag} .= $note.",".$amount;
        } else {
     $hash{$tag} = $note.",".$amount;
     }
    }
     print "Total = $total\n\n";
  return(\%hash);
};

sub open_files {
my($title,$file) = @_;
print "----< $title >--------\n";
my %hash = ();
my $total = 0;
my $ii = 0;
open ( my $fh, "<", $file ) || die "Flaming death: $?\n";
while (<$fh>){
    my $line = $_;
    if ( $line =~ m/^#+/ ) { #print "Skipping header.\n"; 
                              next; };
    chomp($line);
    #print "\$line: $line\n";
    my($tag,$note,$amount) = split(/\,/,$line);
     $ii++;
     print "$ii: $tag = $amount\n";
     $total += $amount;
     if( $hash{$tag} ){
         $hash{$tag} += $amount;
        } else {
     $hash{$tag} = $amount;
     }
    }
     print "Total = $total\n\n";
  close $fh;
  return(\%hash);
}

