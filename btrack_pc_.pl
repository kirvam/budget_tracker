use strict;
use warnings;
use Data::Dumper;
use Sort::Naturally;
###

my $budget_file =$ARGV[0];
my $entries_file =$ARGV[1];

print "\n";
my $hash_ref = open_files("Budget",$budget_file);
my $entry_hash_ref = open_files("Entries",$entries_file);
print Dumper \$entry_hash_ref;

## Need to create sub to subtract Entries from Budget.
print_hash("Total of Entries",$entry_hash_ref);

open_detail_budget("Budget Detail",$budget_file);
print "\n";
my $make_entries_href = make_entries_hash($entries_file);

report_status("Current Status",$hash_ref,$entry_hash_ref,$make_entries_href);

# Subs
#####
sub get_date{
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) =
                                                localtime(time);
$year += 1900;
$mon += 1;
$mday = sprintf("%02d", $mday);
my $date = $year."-".$mon."-".$mday;
return ($date);
};

sub make_entries_hash{
my($file) = @_;
my $title = "make_entries_hash";
###
	print "----< $title >--------\n";
	my %hash = ();
	my $total = 0;
	my $ii = 0;
	open ( my $fh, "<", $file ) || die "Flaming death: $?\n";
	while (<$fh>){
	    my $line = $_;
	    if ( $line =~ m/^#+/ ) { #print "Skipping header.\n";
				      next; };
	    chomp($line);
	    print "\$line: $line\n";
          # LINE NUM/ID,#2019,#Account Code,#TAG,#Manager Description,#PC Unique Sorter1
          #
	    my($tag,$amount,$acct,$note,$lnote,$sorter) = split(/\,/,$line);
            $tag = $tag."-".$note;
	     $ii++;
	     print "$ii: $tag = $amount\n";
	     $total += $amount;
	     if( $hash{$tag} ){
		 $hash{$tag} .= "|".$line ;
		} else {
	     $hash{$tag} =  $line ;
	     }
	    }
	     print "Total = $total\n\n";
          print "DUMPER in make_entries_hash\n";
          print Dumper \%hash;
	  return(\%hash);
          close $fh;
###

};

sub report_status {
# make entries against budget with entries
my($title,$budget_href,$entry_href,$make_entries_href) = @_;
my $outfile = "pc_status_file_";
my $fdate = get_date();
$outfile = $outfile."-".$fdate."_.csv";
my $toutfile = "test-".$outfile;
open ( my $fh, ">", $outfile) || die "Flaming death on open of: $outfile: $?\n\n";
open ( my $tfh, ">", $toutfile) || die "Flaming death on open of: $toutfile: $?\n\n";

print $fh "#,budget-item[line],org budget, entry cur total,BAL,Note\n";
print $tfh "#,budget-item[line],org budget, encumbered/cur total,BAL,Note\n";

my $ii = 0;
my $total = 0;
my $stotal = 0;
my $tbtotal = 0;
print "-----< $title >----\n";
for my $entry ( nsort keys %{ $budget_href } ){
   $ii++;
   # print budget
   print "$ii,$entry, ${$budget_href}{$entry}\n";
   my $btotal = 0;
   $btotal = ${$budget_href}{$entry};
   $tbtotal += ${$budget_href}{$entry};
   print $fh "$ii,$entry, ${$budget_href}{$entry}\n";
   print $tfh "$ii,$entry, ${$budget_href}{$entry},";
      
   #print "  : $entry = ${$entry_href}{$entry}\n";
        
   if ( ${$entry_href}{$entry} ){ 
         $total = ${$budget_href}{$entry} - ${$entry_href}{$entry};
       } else {
          $total = 0;
       };
   #$entries =~ s/\|/\n \,\,/;
   #print " ,";
   #print "$entries\n";
   #print $fh "$entries\n";
   if ( ${$entry_href}{$entry}){ 
        print "  ,$entry, , ${$entry_href}{$entry}\n";
        print $fh "  ,$entry, ,${$entry_href}{$entry}\n";
        print $tfh "${$entry_href}{$entry},$total\n";

       } else {
         print "  ,$entry, ,NO ENTRIES\n";
         print $fh "  ,$entry, ,NO ENTRIES\n";
         print $tfh "0,$total,NO ENTRIES\n";

       }; 


          print "  ,BAL, , ,$total\n";
          print $fh "  ,BAL, , ,$total\n";
          $stotal += $total;
          $total = 0;
           #my $entries;
           #my @array;
           if ( ${$make_entries_href}{$entry} ){
               #$entries =~ s/\|/\n \,\,/;
               my $entries = ${$make_entries_href}{$entry};
               my @array = fix_entries($entries);
               # sub fix_entries;
               # my @array = fix_entries($string);
                foreach my $item ( 0 .. $#array ){
                  print ",$array[$item]\n";
                  print $fh ",$array[$item]\n";
                  print $tfh ",$array[$item]\n";
                   };
                } else {
                     print "NO ENTRIES\n";
              };
  # print " ,";
  # print $fh " ,";
  # print $tfh " ,";
  # print "$entries\n";
  # print $fh "$entries\n";
  # print $tfh "$entries\n";
    };
   print $fh " ,  ,$tbtotal, ,$stotal\n";
   print $tfh " ,  ,$tbtotal, ,$stotal\n";
};


sub fix_entries {
my($string) = @_;
my @array = split(/\|/,$string);
my @new;
foreach my $item ( 0 .. $#array ){
     my($id,$val,$acc,$tag,$note) = split(/,/,$array[$item]);
     my $new_id = $id."-".$tag;
     print "$new_id, $val, $note\n";
     push @new, "$new_id, $val, $note";

  };
 return @new;
};

###
sub print_hash {
# sub to print hash
my($title,$hashref) = @_;
my $ii = 0;
my $total = 0;
print "----< $title >--------\n";
for my $entry ( nsort keys %{ $hashref } ){
   $ii++;
   print "$ii: $entry = ${$hashref}{$entry}\n";
   $total += ${$hashref}{$entry};
 }
   print "Total: $total\n";
};

sub open_detail_budget {
my($title,$file) = @_;
print "\n----< $title >--------\n";
my %hash = ();
my $total = 0;
my $ii = 0;
open ( my $fh, "<", $file ) || die "Flaming death: $?\n";
while (<$fh>){
    my $line = $_;
    if ( $line =~ m/^#+/ ) { #print "Skipping header.\n"; 
                              next; };
    chomp($line);
    #print "\$line: $line\n";
  #  my($tag,$note,$amount) = split(/\,/,$line);
  #  adding $org which is 2019 recommended, amount is 2019 adjusted
  my($tag,$amount,$acct,$note,$lnote,$sorter) = split(/\,/,$line);
     $tag = $tag."-".$note;

     $ii++;
     print "$ii: $tag = $amount, $lnote\n";
     $total += $amount;
     if( $hash{$tag} ){
         $hash{$tag} .= $note.",".$amount;
        } else {
     $hash{$tag} = $note.",".$amount;
     }
    }
     print "Total = $total\n\n";
  return(\%hash);
};

sub open_files {
my($title,$file) = @_;
print "----< $title >--------\n";
my %hash = ();
my $total = 0;
my $ii = 0;
open ( my $fh, "<", $file ) || die "Flaming death: $?\n";
while (<$fh>){
    my $line = $_;
    if ( $line =~ m/^#+/ ) { #print "Skipping header.\n"; 
                              next; };
    chomp($line);
    #print "\$line: $line\n";
   # my($tag,$note,$amount) = split(/\,/,$line);
    my($tag,$amount,$acct,$note,$lnote,$sorter) = split(/\,/,$line);
    if ($note){ 
         $tag = $tag."-".$note;
          };
     $ii++;
     print "$ii: $tag = $amount\n";
     $total += $amount;
     if( $hash{$tag} ){
         $hash{$tag} += $amount;
        } else {
     $hash{$tag} = $amount;
     }
    }
     print "Total = $total\n\n";
  close $fh;
  return(\%hash);
}

