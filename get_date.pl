use strict;
use warnings;

my $date = get_date();
print "\$date: $date\n";


sub get_date{
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) =
                                                localtime(time);
$year += 1900;
#print $year,"\n";
$mon += 1;
#$year = sprintf("%02d", $year % 100);
$mday = sprintf("%02d", $mday);
my $date = $year."-".$mon."-".$mday;
return ($date);
}
